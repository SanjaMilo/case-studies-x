import React, { Component } from 'react';
import Card from './Card';

class List extends Component {
    constructor(props) {
        super(props)
    };

    render() {
        return (
            <div className="List row">
                {this.props.cards.map((el, i) => (<Card checked={this.props.checked} key={i} cardImg={require(`../assets/images/${el.image}.jpg`)} cardTitle={el.title} cardTitle2={el.title2} cardBody={el.body} cardLessons={el.lessons} />))}
            </div>
        )
    };
};

export default List;


