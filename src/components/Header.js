import React, { useState } from 'react';
import brainster_logo from '../assets/images/Brainster-Logo.png';
import { Button, Modal, InputGroup, FormControl } from 'react-bootstrap';

const Header = (props) => {
	const [ show, setShow ] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [ open, isOpened ] = useState(false);

	return (
		<div className="Header">
			<a href="/">
				<div className="logo">
					<img src={brainster_logo} alt="Logo" />
				</div>
			</a>
			<nav className="nav-links">
				<ul className="ul-links">
					<li>
						<a className="a-links" href="https://brainster.co/courses?type=all" target="_blank">
							<p>Академии</p>
						</a>
					</li>
					<li>
						<a className="a-links" href="https://online.brainster.co/" target="_blank">
							<p>Вебинари</p>
						</a>
					</li>
					<li>
						<a
							className="a-links"
							href="https://blog.brainster.co/category/%d0%ba%d0%b0%d1%80%d0%b8%d0%b5%d1%80%d0%b0/"
							target="_blank"
						>
							<p>Тест за кариера</p>
						</a>
					</li>
					<li>
						<a className="a-links" href="https://blog.brainster.co/edukacija-nikolina/" target="_blank">
							<p>Блог</p>
						</a>
					</li>
					<li>
						<Button variant="primary" onClick={handleShow} className="btn btn-form-popup">
							Пријави се
						</Button>
					</li>
				</ul>	

				<ul className="ul-links-menu" style={ open ? {display: "block"} : {display: "none"}}>
					<li>
						<a className="a-links-menu" href="https://brainster.co/courses?type=all" target="_blank">
							<p>Академии</p>
						</a>
					</li>
					<li>
						<a className="a-links-menu" href="https://online.brainster.co/" target="_blank">
							<p>Вебинари</p>
						</a>
					</li>
					<li>
						<a
							className="a-links-menu"
							href="https://blog.brainster.co/category/%d0%ba%d0%b0%d1%80%d0%b8%d0%b5%d1%80%d0%b0/"
							target="_blank"
						>
							<p>Тест за кариера</p>
						</a>
					</li>
					<li>
						<a className="a-links-menu" href="https://blog.brainster.co/edukacija-nikolina/" target="_blank">
							<p>Блог</p>
						</a>
					</li>
					<li>
						<Button variant="primary" onClick={handleShow} className="btn btn-form-popup">
							Пријави се
						</Button>
					</li>
				</ul>
			</nav>

			<Modal size="lg" className="text-center" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<h5 className="modal-text">Пријави се за да добиваш известувања на меил:</h5>
				</Modal.Header>
				<Modal.Body>
					<InputGroup className="mb-3">
						<FormControl
							className="input-modal"
							placeholder="Email Address"
							aria-label="Recipient's username"
							aria-describedby="basic-addon2"
						/>
						<InputGroup.Append>
							<Button variant="outline-secondary">Пријави се</Button>
						</InputGroup.Append>
					</InputGroup>
					Можеш да се исклучиш од листата на меилови во секое време!
				</Modal.Body>
			</Modal>

			<button onClick={() => isOpened(!open)} id="close-menu-btn">
					<span class="material-icons close-menu">sort</span>
			</button>
		</div>
	);
};

export default Header;
